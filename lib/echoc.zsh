# echo wrapper to allow colored output
function echoc {
  setopt ERR_RETURN # to return from this function if zparseopts fails

  local foreground background no_newline to_stderr show_help
  zparseopts -D \
    f:=foreground \
    b:=background \
    n=no_newline \
    e=to_stderr \
    h=show_help

  if [[ $show_help == '-h' ]] then
    cat <<END_OF_HELP
  -f  foreground color as number
  -b  background color as number
  -n  do not print trailing newline
  -e  print to STDERR

  -h  help (this text)
END_OF_HELP
    return 0
  fi

  local foreground_color="$(/usr/bin/tput setaf $foreground[2])"
  local background_color="$(/usr/bin/tput setab $background[2])"
  local reset_colors="$(/usr/bin/tput op)"

  if [[ -n $to_stderr ]] then
    1>&2 echo ${no_newline[1]} "${foreground:+$foreground_color}${background:+$background_color}$argv$reset_colors"
  else
    echo ${no_newline[1]} "${foreground:+$foreground_color}${background:+$background_color}$argv$reset_colors"
  fi
}
