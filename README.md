# Android Backup

This is a set of [Zsh](https://www.zsh.org/) scripts and [Platypus](https://sveinbjorn.org/platypus) wrapper applications to make it easy to backup an android phone in an automated way, without any cloud bullcrap.

For SSH and Rsync access we are using [SimpleSSHD](http://www.galexander.org/software/simplesshd/) ([source code](http://galexander.org/git/simplesshd.git/)) ([install from F-Droid](https://f-droid.org/en/packages/org.galexander.sshd/)).

## Setup

Anywhere you see `<PHONE_NAME>` replace it with name of your phone.

### Install SimpleSSHD on Phone

Install [SimpleSSHD](https://f-droid.org/en/packages/org.galexander.sshd/) from [F-Droid](https://f-droid.org/en/).

### Install Dependencies on Computer

Ensure that you have `ssh` and `rsync` installed.

### Configure SSH

#### Setup `hosts` File

Add your phone name to the hosts file with the appropriate IP address.

```hosts
192.168.?.?	<PHONE_NAME>
```

#### Setup SSH Config

Add the following:

```ssh_config
Host <PHONE_NAME>
  HostKeyAlias <PHONE_NAME>
  IdentityFile ~/.ssh/<PHONE_NAME>_key
  Port 2222
```

To `~/.ssh/config`

#### Generate SSH Key

```zsh
ssh-keygen -t rsa -b 4096 -o -a 100 -f ~/.ssh/<PHONE_NAME>_key
```

TODO: add ecdsa key generation command

#### Put SSH Key on the Phone

```zsh
cat ~/.ssh/<PHONE_NAME>_key.pub | ssh -o 'PasswordAuthentication yes' <PHONE_NAME> 'cat >> authorized_keys'
```

or

```zsh
scp -o 'PasswordAuthentication yes' ~/.ssh/<KEY_NAME>.pub <PHONE_NAME>:authorized_keys
```

#### Restore SELinux Context

Login to the phone and, from the SimpleSSHD home directory (e.g.  `/data/user/0/org.galexander.sshd/files`) run:

```zsh
restorecon -F authorized_keys
```

#### Test Login

```zsh
ssh -v -i ~/.ssh/<PHONE_NAME>_key <PHONE_NAME>
```

### Find sdcard Path from Phone

Use [Android Debug Bridge (adb)](https://developer.android.com/studio/command-line/adb) to find the path of your external storage card.

```zsh
adb shell ls /storage
```

### Create Platypus app(s)

### Review Exclude Paths

Review the exclude paths in [android_excludes.rules](android_excludes.rules) and make any necessary changes.

## Scripts

[android-adb](bin/android-adb.zsh) is the original script that uses [Android Debug Bridge (adb)](https://developer.android.com/studio/command-line/adb) to copy files from your phone over a USB cable.

`adb` can be obtained by installing the [Android SDK Platform Tools](https://developer.android.com/studio/releases/platform-tools.html).

Alternately, you can install the platform tools with Homebrew:

```zsh
brew cask install android-platform-tools
```
