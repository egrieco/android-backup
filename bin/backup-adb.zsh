# change foreground color
function echo-fg {
  echo $(/usr/bin/tput setaf $1)"$argv[2,$#]"$(/usr/bin/tput setaf 0)
}

# change background color
function echo-bg {
  echo $(/usr/bin/tput setab $1)"$argv[2,$#]"$(/usr/bin/tput setab 0)
}

# usage:
# backup-phone DISK SDCARD_ID PHONE_NAME
function backup-phone {
  local backup_date=$(date +'%F')
  local backup_disk="${argv[1]:?Please provide a disk name}"
  local phone_name="${argv[3]:?Please provide a name for your phone}"
  local backup_dir_base="/Volumes/${backup_disk}/Backup/${phone_name}"
  local backup_dir="${backup_dir_base}/${backup_date}"
  local backup_command='adb pull -a'

  # android is weird about naming the internal storage vs the sdcard, they are reversed
  local storage_source_path='/sdcard'
  local storage_dest_path="${backup_dir}/internal"
  local sdcard_source_path="/storage/${argv[2]:?Please provide the unique sdcard path id of your phone}"
  local sdcard_dest_path="${backup_dir}/external"

  if [[ -d ${backup_dir_base} ]] then
    if [[ -e ${storage_dest_path} || -e ${sdcard_dest_path} ]] then
      echo-fg 1 "One or both of the destination paths already exists at: ${backup_dir}"
      echo "Delete 'internal' and/or 'external' and try again."
    else
      mkdir -p "${storage_dest_path}"
      mkdir -p "${sdcard_dest_path}"

      echo-fg 6 "Backing up phone storage to: ${storage_dest_path}..."
      local storage_command="${backup_command} \"${storage_source_path}\" \"${storage_dest_path}\""
      echo "$storage_command"
      eval "$storage_command"

      echo-fg 6 "Backing up phone sdcard to: ${sdcard_dest_path}..."
      local sdcard_command="${backup_command} \"${sdcard_source_path}\" \"${sdcard_dest_path}\""
      echo "$sdcard_command"
      eval "$sdcard_command"

      echo-fg 2 "Done backing up phone to: ${backup_dir}"
    fi
  else
    echo-fg 3 "Please attach the disk: ${backup_disk}"
  fi
}
