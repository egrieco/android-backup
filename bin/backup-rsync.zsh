export RSYNC_BIN='/usr/local/bin/rsync'

# backup android device
function backup-phone {
  local phone_name_arg backup_root external_path hardlink_files dry_run verbose show_help
  zparseopts -D -E \
    p:=phone_name_arg \
    d:=backup_root \
    e:=external_path \
    -exclude-from:=exclude_from \
    l=hardlink_files \
    n=dry_run \
    v=verbose \
    h=show_help

  if [[ $show_help == '-h' ]] then
    cat <<END_OF_HELP
  -p  phone_name NAME
  -d  backup_root PATH  e.g. /Volumes/DISK/Backup/PHONE
  -e  external_path PATH
  --exclude-from  exclude_from PATH

  -l  hardlink_files
  -n  dry run
  -v  verbose

  -h  help (this text)
END_OF_HELP
    return 0
  fi

  unsetopt ERR_RETURN # to ensure that we can capture errors below

  echo "phone_name    = $phone_name_arg[2]"
  echo "backup_root   = $backup_root[2]"
  echo "external_path = $external_path[2]"
  echo "exclude-from:   $exclude_from[@]"
  if [[ -n $hardlink_files ]] echo 'hard link mode enabled'
  if [[ -n $dry_run ]] echo 'dry run enabled'
  if [[ -n $verbose ]] echo 'verbose mode enabled'

  # set variables
  local backup_path="${backup_root[2]:?Please provide a base path in which to backup your phone}"
  local phone_name="${phone_name_arg[2]:?Please provide a name for your phone}"
  local phone_hostname="${phone_name}"
  local internal_path_remote="${phone_hostname}:/sdcard/"
  local external_path_remote="${phone_hostname}:/storage/${external_path[2]}/"
  # :TODO: allow absolute paths and prefix /Volumes if a disk name is provided
  local current_date=$(date +'%F')
  local current_backup_path="$backup_path/$current_date"
  local current_link_name='latest'
  local partial_dir="/tmp/rsync-partial-phone-${phone_name}-bak"
  local backup_command="$RSYNC_BIN -amhP --stats --no-i-r --partial-dir=$partial_dir"
  local excludes="$exclude_from"
  local last_backup_date_file='.last_backup_date'
  local last_backup_date_path="$backup_path/$last_backup_date_file"
  local latest_backup_path="$backup_path/$current_link_name"

  # check if backup_path is mounted, abort if not
  if [[ ! -d $backup_path ]] then
    echoc -f 1 "Backup path does not exist or isn't mounted: $backup_path"
    return 1
  fi

  if [[ -d $partial_dir ]] then
    echoc -f 4 "Partial dir exists"
  else
    echoc -f 4 "Partial dir does not exist. Creating it..."
    mkdir -pv $partial_dir
  fi

  local internal_path_local external_path_local

  if [[ $hardlink_files == '-l' ]] then
    internal_path_local="$current_backup_path/internal"
    external_path_local="$current_backup_path/external"

    # make the base backup path, this is necessary for hard linking to work properly with the logic below
    if [[ -n $verbose ]] echoc -f 2 "Ensure that the backup path exists: $current_backup_path"
    if [[ -z $dry_run ]] mkdir -p $current_backup_path

    # save space by hard linking already existing files (like TimeMachine on macOS)
    local previous_backup_dir=$(ls $backup_path | rg '^\d{4}\-\d{2}\-\d{2}$' | tail -n 2 | head -n 1)
    local link_dest_int link_dest_ext
    # check if previous_backup_dir is empty or equal to the current date
    if [[ -z $previous_backup_dir || $previous_backup_dir == $current_date ]] then
      link_dest_int=''
      link_dest_ext=''
      echoc -f 3 "No previous backup found. Skipping link dest. ($previous_backup_dir)"
    else
      # :TODO: test that link-dest is a directory
      # :TODO: ensure that backup paths are properly quoted
      link_dest_int="--link-dest=$backup_path/$previous_backup_dir/internal"
      link_dest_ext="--link-dest=$backup_path/$previous_backup_dir/external"
      echo 'Hard linking to existing files with:'
      echoc -f 3 $link_dest_int
      echoc -f 3 $link_dest_ext
    fi

    # construct commands
    local internal_storage_command="$backup_command $excludes $link_dest_int '$internal_path_remote' '$internal_path_local'"
    local external_storage_command="$backup_command $excludes $link_dest_ext '$external_path_remote' '$external_path_local'"
  else
    internal_path_local="$backup_path/$current_link_name/internal"
    external_path_local="$backup_path/$current_link_name/external"

    # remove a "latest" link if it's a symlink
    if [[ -L $latest_backup_path ]] then
      if [[ -n $verbose ]] echoc -f 2 "Removing 'latest' symlink: $latest_backup_path"
      # if [[ -z $dry_run ]] rm -v $latest_backup_path
    fi

    # read last_backup_date_file
    local backup_flags_int=''
    local backup_flags_ext=''
    if [[ ! -r "$last_backup_date_path" ]] then
      echoc -f 1 "Last backup date file does not exist: $last_backup_date_path"
    else
      local last_backup_date="$(cat "$last_backup_date_path")"
      date -j -f '%F' "$last_backup_date" 1>/dev/null 2>/dev/null
      if [[ $? != 0 ]] then
        echoc -f 1 "Last backup date file contains an invalid date: $last_backup_date"
      else
        if [[ -n $verbose ]] echoc -f 2 "Last backup date retrieved: $last_backup_date"
        # :TODO: ensure that backup paths are properly quoted
        # :TODO: ensure that rsync will create the necessary backup paths
        backup_flags_int="--backup --backup-dir=$backup_path/$last_backup_date/internal"
        backup_flags_ext="--backup --backup-dir=$backup_path/$last_backup_date/external"
      fi
    fi

    # construct commands
    local internal_storage_command="$backup_command $excludes $backup_flags_int '$internal_path_remote' '$internal_path_local'"
    local external_storage_command="$backup_command $excludes $backup_flags_ext '$external_path_remote' '$external_path_local'"

    # write last_backup_date_file
    # :TODO: determine if we should make this happen only on completion
    if [[ -n $verbose ]] echoc -f 2 "Write $last_backup_date_file file: $last_backup_date_path"
    if [[ -z $dry_run ]] echo $current_date >"$last_backup_date_path"
  fi

  # execute commands
  if [[ -n $verbose ]] echoc -f 2 "Backing up internal storage from ${phone_name}..."
  echoc -f 2 $internal_storage_command
  if [[ -z $dry_run ]] then
    mkdir -p "$internal_path_local"
    eval $internal_storage_command
  fi

  # only backup SD card if the path is provided
  if [[ -n $external_path ]] then
    if [[ -n $verbose ]] echoc -f 2 "Backing up external storage from ${phone_name}..."
    echoc -f 2 $external_storage_command
    if [[ -z $dry_run ]] then
      mkdir -p "$external_path_local"
      eval $external_storage_command
    fi
  else
    echoc -f 2 "No external path specified"
  fi

  if [[ -n $hardlink_files ]] then
    # remove existing latest dir (ln -sF doesn't seem to work properly)
    # find "$backup_path" -name "$current_link_name" -type l -depth 1 -ls -delete
    if [[ -h "$backup_path/$current_link_name" ]] then
      if [[ -n $verbose ]] echoc -f 2 "Removing previous latest directory link: $backup_path/$current_link_name"
      if [[ -z $dry_run ]] rm -v "$backup_path/$current_link_name"
    fi

    # make "latest" link dir entry
    if [[ -n $verbose ]] echoc -f 2 "Creating latest directory link: $backup_path/$current_link_name"
    if [[ -z $dry_run ]] ln -sv $current_date "$backup_path/$current_link_name"
  fi

  echoc -f 2 "Done backing up '$phone_name'."
}
